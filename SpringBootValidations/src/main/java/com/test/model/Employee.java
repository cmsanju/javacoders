package com.test.model;

public class Employee
{
    private int id;
    private String empName;
    private String empCmp;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpCmp() {
		return empCmp;
	}
	public void setEmpCmp(String empCmp) {
		this.empCmp = empCmp;
	}
    
}